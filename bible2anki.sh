#!/usr/bin/bash

DOWNLOAD=engwebp_usfm.zip
if [[ ! -f $DOWNLOAD ]]; then
    wget -N https://ebible.org/Scriptures/$DOWNLOAD
fi
rm -Rfv books
unzip -u $DOWNLOAD -d books
# These days the books are missing a newline at the end, so we need to add it back in
# If not, the name of the book is not picked up after Genesis.
BOOKS=$(cat books.txt)
for book in $BOOKS; do
    echo >> $book
done
# Concatenate the books together
cat $BOOKS > bible.usfm

usfm2bcv () { # bcv = book/chapter/verse
    while read TYPE VALUE TEXT; do
        if [ $TYPE == 'id' ]; then
            BOOKCODE=$VALUE
        elif [ $TYPE == 'h' ]; then
            if [ "$TEXT" == "" ]; then
                BOOK="$VALUE"
            else
                BOOK="$VALUE $TEXT"
            fi
        elif [ $TYPE == 'c' ]; then
            CHAPTER=$VALUE
        elif [ $TYPE == 'v' ]; then
            VERSE=$VALUE
            # Each line contains this format
            echo -e $BOOKCODE'\t'$BOOK'\t'$CHAPTER'\t'$VERSE'\t'$TEXT
        fi
    done
}

IN=bible.usfm
OUT=bible-verses.tsv

# process usfm tags, and replace preferred language (Good News/gospel)
# perl: -p : loop over the code from -e
#       -e : execute from CLI
#       -0777 : file slurp mode - read a file in one step
# Remove lines starting with: \ide , \mt1 , \mt2 , \mt3 , \nb, \d, \sp, \ms1.
# Join lines that don't start with a backslash
# Remove \f stuff \f*
# Remove \x stuff \x*
# Remove \wj stuff \wj*, but keep the stuff.
# Join lines that don't start with backslash, again
# Remove lines starting with \p, \q1, \q2, \b, and \m
# Remove strongs \w stuff \w* tag, but the stuff is separated by a | and we keep the word before that and throw away the strongs reference.
# Do the same with \wi stuff \wi*, which is used inside quotes in Greek.
# Expand to include \wj “\+w Allow|strong="G863"\+w* formats for direct quotes.
# \wj ... \wj* surrounds the quote, and each word inside the quote is \+w ... \+w*
# Get rid of \x ... \x*
# Replace "Good News" with gospel.
cat $IN \
    | grep -Ev '\\(ide |mt[1-3] |nb|d|sp|ms1)' \
    | perl -0777 -pe 's/\n([^\\])/ \1/g' \
    | perl -0777 -pe 's/\\f.*?\\f\*//g' \
    | perl -0777 -pe 's/\\x.*?\\x\*//g' \
    | perl -0777 -pe 's/\\wj(.*?)\\wj\*/\1/g' \
    | perl -0777 -pe 's/\n([^\\])/\1/g' \
    | perl -0777 -pe 's/\n\\p//g' \
    | perl -0777 -pe 's/\n\\q1//g' \
    | perl -0777 -pe 's/\n\\q2//g' \
    | perl -0777 -pe 's/\n\\b//g' \
    | perl -0777 -pe 's/\n\\m//g' \
    | perl -0777 -pe 's/\\w (.*?)\|.*?\\w\*/\1/g' \
    | perl -0777 -pe 's/\\\+w (.*?)\|.*?\\\+w\*/\1/g' \
    | sed 's/\<Good News\>/gospel/g' \
    | usfm2bcv \
    > $OUT

# Are there any phrases to convert?  This is useful for turning American English WEBP
# into British English while still keeping the YAHWEH (rather than LORD)
IN=bible-verses.tsv
OUT=bible-verses-rephrased.tsv
IFS='|'

generate_replace_code () {
    echo "cat $IN \\"
    while read NOTHING ORIG REPLACE COUNT; do
        echo "| sed 's/$ORIG/$REPLACE/g' \\"
    done
    echo "> $OUT"
}

if [[ -f phrasetranslist.txt ]]; then
    REPLACECODE=$(generate_replace_code < phrasetranslist.txt)
    eval "$REPLACECODE"
    IN="$OUT"
fi


# We're going to output something for Anki:
# * Tab separated
# * By default, Anki assigns Front, Back, and Tags.
# * Tags are space-separated

OUT=bible-anki.tsv
IFS=$'\t'

verse2previous () {
    read BOOKCODE BOOK CHAPTER VERSE TEXT
    CURRENT="$BOOK beginning."
    NEXT="$BOOK $CHAPTER:$VERSE $TEXT"
    BOOKTAG=$(echo $BOOK | tr -d "[:space:]")
    TAGS="WEB $BOOKTAG chapter-$CHAPTER verse-$VERSE"
    echo -e $CURRENT'\t'$NEXT'\t'$TAGS
    PREVBOOKCODE=$BOOKCODE
    while read BOOKCODE BOOK CHAPTER VERSE TEXT; do
        if [[ $BOOKCODE != $PREVBOOKCODE ]]; then
            CURRENT="$BOOK beginning."
        else
            CURRENT="$NEXT"
        fi
        NEXT="$BOOK $CHAPTER:$VERSE $TEXT"
        BOOKTAG=$(echo $BOOK | tr -d "[:space:]")
        TAGS="WEB $BOOKTAG chapter-$CHAPTER verse-$VERSE"
        echo -e $CURRENT'\t'$NEXT'\t'$TAGS
        PREVBOOKCODE=$BOOKCODE
    done
}
verse2previous < $IN > $OUT
