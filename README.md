# README

Bible2Anki is a script (and a little data) for creating an Anki deck that helps you memorise passages of the Bible.  I've set it up to use the World English Bible (American, Protestant books), but then convert American phrases to British ones in order to keep the YAHWEH phrases while retaining British spelling.

Each Anki card has a verse on the front and the following verse on the back.  All notes are tagged with:

* WEB
* The Bible book (e.g. Exodus)
* The chapter (e.g. chapter-20)
* The verse (e.g. verse-1)

This allows you to learn a chapter at a time, or verse 3:16 from every book of the Bible (Donald Knuth did that and wrote a book).

This is not a good deck to use if you just want to memorise special verses that are not consecutive.

## How do I use this code?

1. Download bible2anki.sh and books.txt.  Put them in a new directory by themselves.
2. If you want to do a phrase conversion, download the appropriate phrasetranslist.txt file from https://ebible.org.  Otherwise it'll be ignored.
3. Run bible2anki.sh.
	* If you've decided to use a different WEB version (or a different translation entirely), you'll probably get errors.  No worries, just look in the books directory and adjust books.txt to match.
4. Load the bible-anki.tsv file into Anki and accept its default field assignments.

Too easy huh.

If there are any issues, log them in the [issue tracker](https://bitbucket.org/multixrulz/bible2anki/issues?status=new&status=open) (login required)

## Acknowledgement

This code has been tweaked from the code shared [here](https://ankiweb.net/shared/info/666724993), thanks Greg.